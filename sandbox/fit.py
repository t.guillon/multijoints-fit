import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize as sciopt

def get_best_plane_params(pts, init_guess):
    """ Linear regression to find best (local) plane passing through points
    
    pts: list of dim npts*3, pts to perform linear regression on
    https://stackoverflow.com/questions/12299540/plane-fitting-to-4-or-more-xyz-points
    """
    def f_min(X,p):
        plane_xyz = p[:3]
        distance = (plane_xyz*X).sum(axis=1) + p[3]
        return distance / np.linalg.norm(plane_xyz)

    def residuals(params, signal, X):
        return f_min(X, params)
    
    sol = sciopt.leastsq(residuals, init_guess, args=(None, pts))[0]
    return sol
    
def get_best_plane_params_opt(pts, init_guess):
    """ Optimizing to find best (local) plane passing through points
    checking that same plane as get_best_plane_params is returned -> YES
    
    pts: list of dim npts*3, pts to perform linear regression on
    https://stackoverflow.com/questions/12299540/plane-fitting-to-4-or-more-xyz-points
    """
    def f_min(X,p):
        plane_xyz = p[:3]
        distance = (plane_xyz*X).sum(axis=1) + p[3]
        distance /= np.linalg.norm(plane_xyz)
        rmse = np.sqrt(np.dot(distance, distance))
        return rmse

    def residuals(params, X):
        return f_min(X, params)
    
    sol = sciopt.minimize(residuals, init_guess, args=pts).x
    return sol
    
def continuity_on_third_joint(xs, params_third, zref):
    a1,b1,c1,d1,,a2,b2,c2,d2 = xs
    alpha, beta, gamma, delta = params_third
    return  (
        (   a2*(d1 + c1*zref) - a1*(d2 + c2*zref)
            )*beta
        + (
            -d1*alpha - c1*zref*alpha + a1*(zref*gamma + delta)
            )*b2
        + (
            d2*alpha + c2*zref*alpha - a2*(zref*gamma + delta)
            )*b1
        )


# Generate demo plane
demo_params = (-0.5, 1., 2., 0.5)
a, b, c, d = demo_params

nvec = np.array((a,b,c))
xor = 0
yor = 0
zor = -d/c
orig = np.array((xor, yor, zor))

def get_zs(xs,ys,params):
    a, b, c, d = params
    coos = np.vstack((xs,ys)).T
    return -1./c*(np.matmul(coos, (a,b))+d)

# Generate points on plane
npts = 100
xs = np.linspace(-10,10,npts)
ys = np.linspace(-10,10,npts)
np.random.shuffle(ys)
zs = get_zs(xs,ys,demo_params)
coos = np.vstack((xs,ys,zs)).T
assert np.allclose([a*x+b*y+c*z+d for x,y,z in coos], 0.)
# Add noise
ampl = 1.e-1
randoms = (2*np.random.rand(len(coos))-1)*ampl
noise_vecs = randoms[:,None]*nvec
coos_noisy = coos + noise_vecs

# Fitting
guess = [1,1,1,None]
mean = np.mean(coos_noisy, axis=0)
d_guess = -np.dot(guess[:3], mean)
guess[-1] = d_guess
params_fit = get_best_plane_params(coos_noisy, demo_params)
print(params_fit)
params_fit_opt = get_best_plane_params_opt(coos_noisy, demo_params)
print(params_fit_opt)

fig = plt.figure()
ax = fig.add_subplot(projection='3d')
ax.scatter(*coos.T, label='init')
ax.scatter(*coos_noisy.T, label='noisy')
ax.legend()
plt.show()

