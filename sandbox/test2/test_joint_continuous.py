# netsh ip all reset
# net sh winsock reset

# poly brick -1 1 -1 1 -1 1
# jset dip 90 dd 45
# hide range plane dip 90 dd 45 below
# jset dip 45 dd 180
# find
# hide range plane dip 90 dd 45 above
# jset dip 45 dd 90
# find

import numpy as np

import scipy.optimize as sciopt

def get_best_plane_params(pts, init_guess):
    """ Linear regression to find best (local) plane passing through points
    
    pts: list of dim npts*3, pts to perform linear regression on
    https://stackoverflow.com/questions/12299540/plane-fitting-to-4-or-more-xyz-points
    """
    def f_min(X,p):
        plane_xyz = p[:3]
        distance = (plane_xyz*X).sum(axis=1) + p[3]
        return distance / np.linalg.norm(plane_xyz)

    def residuals(params, signal, X):
        return f_min(X, params)
    
    sol = sciopt.leastsq(residuals, init_guess, args=(None, pts))[0]
    return sol
    
def get_best_plane_params_opt(pts, init_guess):
    """ Optimizing to find best (local) plane passing through points
    checking that same plane as get_best_plane_params is returned -> YES
    
    pts: list of dim npts*3, pts to perform linear regression on
    https://stackoverflow.com/questions/12299540/plane-fitting-to-4-or-more-xyz-points
    """
    def f_min(X,p):
        plane_xyz = p[:3]
        distance = (plane_xyz*X).sum(axis=1) + p[3]
        distance /= np.linalg.norm(plane_xyz)
        rmse = np.sqrt(np.dot(distance, distance))
        return rmse

    def residuals(params, X):
        return f_min(X, params)
    
    sol = sciopt.minimize(residuals, init_guess, args=pts).x
    return sol
    
def get_best_multiplane_params_opt(pts, init_guess, abcd3):
    """ Optimizing to find best (local) planes passing through points
    
    pts: list of nplanes of (npts*3)-arrays of , pts to fit per plane
    """
    def f_min(X,p):
        plane_xyz = p[:3]
        distance = (plane_xyz*X).sum(axis=1) + p[3]
        distance /= np.linalg.norm(plane_xyz)
        rmse = np.sqrt(np.dot(distance, distance))
        return rmse

    def residuals_single_plane(params, X):
        return f_min(X, params)
        
    def residuals(params, X):
        res = 0
        nplanes = len(X)
        params=params.reshape((nplanes,-1))
        for pp,xx in zip(params, X):
            res += residuals_single_plane(pp,xx)
        return res
    
    
    def matrank(params, abcd3=abcd3, ijunction=0):
        mat = np.vstack((
            params.reshape((-1,4))[ijunction:ijunction+2],
            abcd3
            ))
        return np.linalg.matrix_rank(mat[:,:3])
    
    def augmatrank(params, abcd3=abcd3, ijunction=0):
        augmat = np.vstack((
            params.reshape((-1,4))[ijunction:ijunction+2],
            abcd3
            ))
        augmat[:,3] *= -1
        return np.linalg.matrix_rank(augmat)
    
    def unitvec1(params):
        return np.linalg.norm(params[:3])
        
    def unitvec2(params):
        return np.linalg.norm(params[4:4+3])
        
    constrain_matrank = sciopt.NonlinearConstraint(matrank, 2, 2)
    constrain_augmatrank = sciopt.NonlinearConstraint(augmatrank, 2, 2)
    constrain_unitvec1 = sciopt.NonlinearConstraint(unitvec1, 1, 1)
    constrain_unitvec2 = sciopt.NonlinearConstraint(unitvec2, 1, 1)
    sol = sciopt.minimize(
        residuals, init_guess, args=pts,
        constraints=(
            constrain_matrank,
            constrain_augmatrank,
            constrain_unitvec1,
            constrain_unitvec2
            ),
        method='trust-constr'
        ).x
    return sol

def get_normal_from_dipdd(dip, dipdir):
    """ Computes normal unit vector of a plane
    
    dip:  int/float, plane dipping angle in degrees
    dipdir:  int/float, plane dipdir angle in degrees
    
    Returns: unit normal vector as np.ndarray
    
    """
    multi_values = np.ndim(dip) > 0
    assert np.shape(dip) == np.shape(dipdir), ( 'dip and dipdir must be the'
                                                'same shape'
                                                )
    dip = np.deg2rad(dip)
    dipdir = np.deg2rad(dipdir)
    nvecs = np.array((  np.sin(dip)*np.sin(dipdir),
                        np.sin(dip)*np.cos(dipdir),
                        np.cos(dip),
                        ))
    
    return nvecs.T if multi_values else nvecs
                        
def get_dipdd_from_normal(normal_vec):
    """ Computes dip and dip direction of a plane
    
    normal_vec: list/tuple/np.ndarray, 3D coordinates of plane unit normal.
                Can be a (Npoints*3) array for multiple computations at once.
    
    Returns: dip and dip direction angles in degrees
    
    """
    multi_values = np.ndim(normal_vec) > 1
    nx, ny, nz = np.transpose(normal_vec)
    dd = np.arctan2(nx, ny)
    dip = np.arctan2(np.linalg.norm((nx, ny), axis=0), nz)
    downard_nvecs = dip > np.pi/2
    if np.any(downard_nvecs):
        if isinstance(dip, np.ndarray):
            dip[downard_nvecs] = np.pi - dip[downard_nvecs]
            dd[downard_nvecs] += np.pi
        else:
            dip = np.pi - dip
            dd += np.pi
    dd %= 2*np.pi
    
    return np.rad2deg((dip, dd)).T if multi_values else np.rad2deg((dip, dd))
    
def get_plane_cartesian_parameters(pt, normal_vec=None):
    """ Finding cartesian parameters of a plane
    
    Cartesian parameters (a,b,c,d) set equation ax+by+cz+d=0
    
    pt: list/tuple/np.ndarray, 3D coordinates of a point on the plane
        If 3x3 container, 3D coordinates of 3 points on the plane.
    normal_vec: list/tuple/np.ndarray, 3D coordinates of plane unit normal
                Ignored if pt gives three points.
    
    Returns: all four plane cartesian parameters as an array
    
    """
    abcd = np.empty(4)
    pt = np.array(pt)
    if pt.ndim == 2:
        vecs = pt[1:]-pt[0]
        normal_vec = np.cross(*vecs)
        normal_vec /= np.linalg.norm(normal_vec)
        if normal_vec[2] < 0:
            normal_vec *= -1
        pt = pt[0]
    abcd[:3] = normal_vec
    abcd[-1] = -np.dot(pt, normal_vec)
    return abcd

def get_zs(xs,ys,params):
    a, b, c, d = params
    coos = np.vstack((xs,ys)).T
    return -1./c*(np.matmul(coos, (a,b))+d)

INVSQRT2 = 1./np.sqrt(2)
INVSQRT3 = 1./np.sqrt(3)

origin = np.zeros(3)
n1 = np.array((0., -INVSQRT2, INVSQRT2))
n2 = np.array((INVSQRT2, 0., INVSQRT2))
n3 = np.array((INVSQRT2, INVSQRT2, 0.))
mat1 = np.vstack((n1,n2,n3))
mat2 = np.insert(mat1,3,origin,axis=1)
abcd1, abcd2, abcd3 = mat2

def random(n,a,b):
    return np.random.random(n)*(b-a) + a
def bisect(x):
    return -x
npts = 25
# Joint 1
ys = np.sort(random(npts, -1, 1))
xs = random(npts, bisect(ys), 1)
zs = get_zs(xs, ys, abcd1)
pts1 = np.vstack((xs,ys,zs)).T
# Joint 2
xs = np.sort(random(npts, -1, 1))
ys = random(npts, -1, bisect(xs))
zs = get_zs(xs, ys, abcd2)
pts2 = np.vstack((xs,ys,zs)).T

# Fitting
guess = [None, None, None, None]
guess = [None, None, None, None]
guess[:3] = (n1+n2)/2
mean = np.mean(pts1+pts2, axis=0)
d_guess = -np.dot(guess[:3], mean)
guess[-1] = d_guess
params_fit = get_best_plane_params(pts1, guess)
params_fit_opt = get_best_plane_params_opt(pts1, guess)
print('Fit lstsq', params_fit)
print('Fit opt', params_fit_opt)

# # Fitting (multi)
# fits = get_best_multiplane_params_opt(
    # [pts1, pts2],
    # np.hstack([guess, guess]),
    # abcd3
    # )
# fit1,fit2 = fits.reshape((-1,4))
# augmat = np.vstack((fit1,fit2,abcd3))
# augmat[:,-1] *= -1
# mat = augmat[:,:3]

# Fitting (multi2)
n1 = np.array((-INVSQRT3, -INVSQRT3, INVSQRT3))
n2 = np.array((INVSQRT3, INVSQRT3, INVSQRT3))
guess1 = [None, None, None, 0] 
guess2 = [None, None, None, 0] 
guess1[:3] = n1
guess2[:3] = n2
fits = get_best_multiplane_params_opt(
    [pts1, pts2],
    np.hstack([guess1, guess2]),
    abcd3
    )
fit1,fit2 = fits.reshape((-1,4))

# Checking
def matrank(params, abcd3=abcd3, ijunction=0):
    mat = np.vstack((
        params.reshape((-1,4))[ijunction:ijunction+2],
        abcd3
        ))[:,:3]
    print(mat)
    return np.linalg.matrix_rank(mat)

def augmatrank(params, abcd3=abcd3, ijunction=0):
    augmat = np.vstack((
        params.reshape((-1,4))[ijunction:ijunction+2],
        abcd3
        ))
    augmat[:,3] *= -1
    print(augmat)
    return np.linalg.matrix_rank(augmat)

def unitvec1(params):
    return np.linalg.norm(params[:3])
    
def unitvec2(params):
    return np.linalg.norm(params[4:4+3])

# assert matrank(fits)==2
# assert augmatrank(fits)==2
# assert np.allclose(unitvec1(fits), 1)
# assert np.allclose(unitvec2(fits), 1)

nfit1 = fit1[:3]
dipdd1 = get_dipdd_from_normal(nfit1)
orfit1 = np.array((0,0,-fit1[3]/fit1[2]))
nfit2 = fit2[:3]
dipdd2 = get_dipdd_from_normal(nfit2)
orfit2 = np.array((0,0,-fit2[3]/fit2[2]))

# 3DEC stuff
import itasca as it
scalar_template = 'data scalar create {},{},{} group "{group}"'
# True solution
# it.command("""
    # block create brick test
    # block cut joint-set dip 90 dip-direction 45
    # block hide range plane dip 90 dip-direction 45 below
    # block cut joint-set dip 45 dip-direction 180
    # block hide off
    # block hide range plane dip 90 dip-direction 45 above
    # block cut joint-set dip 45 dip-direction 90
    # block hide off
    # """)
# Fit
it.command(f"""
    block create brick test
    block cut joint-set dip 90 dip-direction 45
    block hide range plane dip 90 dip-direction 45 below
    block cut joint-set dip {dipdd1[0]} dip-direction {dipdd1[1]} origin {tuple(orfit1)}
    block hide off
    block hide range plane dip 90 dip-direction 45 above
    block cut joint-set dip {dipdd2[0]} dip-direction {dipdd2[1]} origin {tuple(orfit2)}
    block hide off
    """)
it.command('program echo off')
scalars = [scalar_template.format(*pos, group='joint=3') for pos in pts1]
it.command('\n'.join(scalars))
scalars = [scalar_template.format(*pos, group='joint=4') for pos in pts2]
it.command('\n'.join(scalars))
it.command('program echo on')