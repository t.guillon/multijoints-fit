# Functions
def constrain1(a1,b1,c1,a2,b2,c2,a3,b3,c3):
    return a1*(
        -a3*b2*c1
        + a2*b3*c1
        + a3*b1*c2
        - a1*b3*c2
        - a2*b1*c3
        + a1*b2*c3
        )
        
def constrain2(a1,b1,c1,a2,b2,c2,a3,b3,c3):
    return c1*(
        a3*b2*c1
        - a2*b3*c1
        - a3*b1*c2
        + a1*b3*c2
        + a2*b1*c3
        - a1*b2*c3
        )
        
    
def constrain3(a1,b1,c1,a2,b2,c2,a3,b3,c3):
    return b2*(
        a3*b2*c1
        - a2*b3*c1
        - a3*b1*c2
        + a1*b3*c2
        + a2*b1*c3
        - a1*b2*c3
        )
        
# Jacobians
def constrain1_jac(a1,b1,c1,a2,b2,c2,a3,b3,c3):
    return np.array((
        (   -a3*b2*c1
            + a2*b3*c1
            + a3*b1*c2
            - a1*b3*c2
            - a2*b1*c3
            + a1*b2*c3
            + a1*(-(b3*c2) + b2*c3)
            ),
        a1*(a3*c2 - a2*c3),
        a1*(-(a3*b2) + a2*b3),
        a1*(b3*c1 - b1*c3),
        a1*(-(a3*c1) + a1*c3),
        a1*(a3*b1 - a1*b3)
        ))

def constrain2_jac(a1,b1,c1,a2,b2,c2,a3,b3,c3):
    return np.array((
        c1*(b3*c2 - b2*c3),
        c1*(-(a3*c2) + a2*c3),
        (   a3*b2*c1
            - a2*b3*c1
            + (a3*b2 - a2*b3)*c1
            - a3*b1*c2
            + a1*b3*c2
            + a2*b1*c3
            - a1*b2*c3
            ),
        c1*(-(b3*c1) + b1*c3),
        c1*(a3*c1 - a1*c3),
        (-(a3*b1) + a1*b3)*c1
        ))
        
def constrain3_jac(a1,b1,c1,a2,b2,c2,a3,b3,c3):
    return np.array((
        b2*(b3*c2 - b2*c3),
        b2*(-(a3*c2) + a2*c3),
        b2*(a3*b2 - a2*b3),
        b2*(-(b3*c1) + b1*c3),
        (   a3*b2*c1
            - a2*b3*c1
            - a3*b1*c2
            + a1*b3*c2
            + a2*b1*c3
            - a1*b2*c3
            + b2*(a3*c1 - a1*c3)
            ),
        b2*(-(a3*b1) + a1*b3)
        ))