# Multi joints-fit

## About
The idea is to fit a piecewise surface to a 3D set of points.

Should be easy, right?
...
Right?


## Content
So far, code is a mess.

I'm still working on the way to achieve the goal.

Code is mostly Python, and a bit of [3DEC](https://www.itascacg.com/software/downloads/3dec-7-00-update) for visualising.

## License
MIT, so do whatever you want.

Still, citation would be appreciated :).
